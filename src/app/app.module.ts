import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { XButtonComponent } from './x-button/x-button.component';
import { XNavBarComponent } from './x-nav-bar/x-nav-bar.component';
import { XTextComponent } from './x-text/x-text.component';


@NgModule({
  declarations: [
    AppComponent,
    XButtonComponent,
    XNavBarComponent,
    XTextComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
