import { Component, OnInit, Attribute, Input } from '@angular/core';

@Component({
  selector: 'app-x-button',
  templateUrl: './x-button.component.html',
  styleUrls: ['./x-button.component.css']
})
export class XButtonComponent implements OnInit {
  caption: string;
  @Input()theme: 'dark' | 'light';

  constructor(@Attribute('caption') caption) {
    this.caption = caption;
  }

  ngOnInit() {
  }

}
