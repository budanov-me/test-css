import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  theme: 'dark' | 'light' = 'dark';
  theme2be: 'dark' | 'light' = 'light';

  toggleTheme(event) {
    this.theme2be = this.theme2be === 'light' ? 'dark' : 'light';
    this.theme = this.theme === 'light' ? 'dark' : 'light';
  }

  ngOnInit(): void {

  }
}
