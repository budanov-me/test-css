import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-x-text',
  templateUrl: './x-text.component.html',
  styleUrls: ['./x-text.component.css']
})
export class XTextComponent implements OnInit {

  @Input()text: string;

  constructor() { }

  ngOnInit() {
  }

}
